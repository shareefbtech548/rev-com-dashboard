#include<iostream>
#include<iomanip>
#include<sstream>
#include<fstream>
#include<string>
#include<curl/curl.h>
#include<jsoncpp/json/json.h>
using namespace std;
size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
	((string*)userdata)->append(ptr, size * nmemb);
	return size * nmemb;
}
int main(int argc, char* argv[])
{
	  // Initialize variables for user input
        string start_date, end_date, Project_id;
        // Prompt user for start date, end date, and repository URL
        cout << "Enter start date (YYYY-MM-DD): ";
        cin >> start_date;
        cout << "Enter end date (YYYY-MM-DD):";
        cin >> end_date;
        cout << "Enter project Id: ";
        cin >> Project_id;
        // Construct GitLab API request URL for comments on a repository
	
	string gitlab_api_url ="https://gitlab.com/api/v4/projects/"+Project_id+"/merge_requests/4/notes?created_after="+start_date+"T00:00:00.000Z&created_before="+end_date+"T23:59:59.999Z";
        cout<< gitlab_api_url << endl;
	// GitLab API endpoint and access token
	string gitlab_access_token = "glpat-eH9hLd8QdyhXBjHdNKqf";
	// CSV file path
	string csv_file_path = "comments.csv";
	// Set up libcurl
	CURL* curl;
	CURLcode res;
	curl = curl_easy_init();
	if (curl)
	{
		// Set the GitLab API endpoint
		curl_easy_setopt(curl, CURLOPT_URL, gitlab_api_url.c_str());
		// Set the GitLab access token
		struct curl_slist *headerlist = NULL;
		string authorization = "Authorization: Bearer " + gitlab_access_token;
		headerlist = curl_slist_append(headerlist, authorization.c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		//disable of ssL
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);


		// Set the callback function for the response
		string response;
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
		// Perform the request
		res = curl_easy_perform(curl);
		if (res == CURLE_OK)
		{
			// Parse the JSON data
			Json::Value root;
			Json::Reader reader;
			bool parsingSuccessful = reader.parse(response, root);
			cout << "JSON data: " << endl;
			cout << response << endl;
			if (parsingSuccessful)
			{
				// Open the CSV file for writing
				ofstream csv_file(csv_file_path);
				// Write the CSV header row
				csv_file <<left <<setw(20) << "Developer Name"<<","
                                         << left <<setw(30) << "Date"<<","
                                         << left <<setw(20) << "Reviewer Name"<<","
                                         << left << setw(20) << "file Type"<<","
                                         << left << setw(30) << "Review Comments"<< endl; 
				// Loop through the discussions and write them to the CSV file
				for (int i = 0; i < root.size(); i++)
				{
					string Developer_Name = root[i]["author"]["name"].asString();
					string Date = root[i]["created_at"].asString();
					string Reviewers_Name =root[i]["author"]["name"].asString();
					string file_Type = root[i]["position"]["new_path"].asString();
					string Review_Comments = root[i]["body"].asString();
					stringstream temp;
					temp << "\"" << Review_Comments << "\"" ;
					csv_file <<left <<setw(20) << Developer_Name<< ","
                                                 <<left <<setw(30) << Date<< ","
                                                 <<left <<setw(20) << Reviewers_Name << ","
                                                 << left <<setw(20) << file_Type<< ","
                                                 << left <<setw(30) << temp.str()<< endl;
				}
				// Close the CSV file
				csv_file.close();
				cout << "comments exported to " << csv_file_path << endl;
			}
			else
			{
				cout << "Error parsing JSON data: " << reader.getFormattedErrorMessages() << endl;
			}
		}
		else
		{
			cout << "Error retrieving comments: " << curl_easy_strerror(res) << endl;
		}
		//Clean up libcurl
		curl_slist_free_all(headerlist);
		curl_easy_cleanup(curl);
	}
	return 0;
}
