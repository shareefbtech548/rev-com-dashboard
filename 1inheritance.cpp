#include <iostream>
#include<stdio.h>  
using namespace std;  
 class Account {  
   public:  
   float salary = 60000;   
 };  
   class Programmer: public Account {  
   public:  
   float bonus = 5000;    
   };       
int main(void) {  
     Programmer p1;  
     cout<<"Salary: "<<p1.salary<<endl;    
     cout<<"Bonus: "<<p1.bonus<<endl;
     // Modifying the values
  p1.salary = 65000;  // Changing the salary
  p1.bonus = 6000;    // Changing the bonus

  // Displaying the modified values
  cout << "Modified Salary: " << p1.salary << endl;    
  cout << "Modified Bonus: " << p1.bonus << endl;    

  return 0;        
}  
